/**
 * Exemplo1: Programacao com threads
 * Autor: Lucio Agostinho Rocha
 * Ultima modificacao: 07/08/2017
 */
package exemplo1;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lucio
 */
public class Jober_Exemplo1  {

        public static void main(String [] args){
            
            final int qtdThreads = 20;
            
            System.out.println("Inicio da criacao das threads.");
            
            //cria o pool de threads
            ExecutorService pool = Executors.newCachedThreadPool();
            
            //Cria cada thread com um novo runnable selecionado
            Thread t1;
            Jober_PrintTasks printTask;
            
            for (int i = 0; i < qtdThreads; i++) {
                printTask = new Jober_PrintTasks("Thread: "+i);
                t1 = new Thread(printTask);
                
                pool.execute(t1);
            }
            
            System.out.println("Threads criadas");
            
            //não aceita novas threads
            pool.shutdown();
            
            try {
                pool.awaitTermination(1, TimeUnit.DAYS);
            } catch (InterruptedException ex) {
                Logger.getLogger(Jober_Exemplo1.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
        }
        
}
